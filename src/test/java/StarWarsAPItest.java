import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import static org.junit.Assert.assertEquals;


public class StarWarsAPItest {

    API api = new API();
    GetRequestRepository repository = new GetRequestRepository(api);

    @org.junit.Test
    //testing the films query
    //this tests the getAll function of the films category.
    public void countAllMoviesTest(){

        JsonObject jsonObject = repository.getAll("films",null);
        System.out.println("jsonObject: "+jsonObject);
        JsonArray results = jsonObject.getAsJsonArray("result");
        System.out.println("results: "+results);
        int actual = results.size();
        String expected = "6";
        assertEquals(expected,actual);
    }
    //
//testing the films query
//this test proves the search function on films works and compares the given release date of the movie "A New Hope"
    //the search term cannot contain spaces.
    //the expected release date of "A New Hope" is May 25 1977. format is not important.
    //the part of the JsonObject you want to get is "release_date"
    //You can use the other tests for information on how to write this test.

    @org.junit.Test
    //testing the films query
    //this test proves the search function on films works and compares the given release date of the movie "A New Hope"
    public void ANewHopeReleaseDateTest() {

    }

        @org.junit.Test
    //testing the planets query
    //this test proves the search function on planets works and compares the given population of the planet Naboo.
    public void NabooPopulationTest(){

        JsonObject jsonObject = repository.getAll("planets", "naboo");
            System.out.println(jsonObject);
            JsonArray results = jsonObject.getAsJsonArray("result");
            System.out.println(results);
            JsonObject planet = results.get(0).getAsJsonObject().getAsJsonObject("properties");
            System.out.println(planet);
        String expected = "4500000000";
        String actual = planet.get("population").getAsString();

        assertEquals(expected,actual);
    }

    @org.junit.Test
    //testing the starships query
    //this test proves the search function on starships works and compares the given model of the Death Star.
    public void DeathStarModelTest(){

        JsonObject jsonObject = repository.getAll("starships", "death");
        JsonArray results = jsonObject.getAsJsonArray("result");
        JsonObject starship = results.get(0).getAsJsonObject().getAsJsonObject("properties");

        String expected = "DS-1 Orbital Battle Station";
        String actual = starship.get("model").getAsString();

        assertEquals(expected,actual);
    }

    @org.junit.Test
    //testing the single getrequest function
    //this test proves that the first guy in the database under people is Luke Skywalker.
    public void GetSingleEntity(){

        JsonObject person = repository.innerRequest("https://www.swapi.tech/api/people/1/").getAsJsonObject("result").getAsJsonObject().getAsJsonObject("properties");
        String expected = "Luke Skywalker";
        String actual = person.get("name").getAsString();

        assertEquals(expected,actual);
    }
}
