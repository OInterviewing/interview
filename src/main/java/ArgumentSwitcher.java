import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ArgumentSwitcher {

    private static final API apiCalls = new API();
    private GetRequestRepository repository = new GetRequestRepository(apiCalls);
    private java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
    Printer printer = new Printer();

    public void switcher(String command, String searchquery) {

        JsonObject jsonObject;

        switch (command) {
            case "films":
                jsonObject = repository.getAll("films", searchquery);
                JsonArray filmresults = jsonObject.getAsJsonArray("results");
                printer.printDetailsFilms(filmresults);
                break;
            case "planets":
                jsonObject = repository.getAll("planets", searchquery);
                JsonArray planetresults = jsonObject.getAsJsonArray("results");
                printer.printDetailsPlanets(planetresults);
                break;
            case "starships":
                jsonObject = repository.getAll("starships", searchquery);
                JsonArray starshipresults = jsonObject.getAsJsonArray("results");
                printer.printDetailsStarships(starshipresults);
                break;
            default:
                System.out.println(command + " is not a available command");
                break;
        }
    }
}
