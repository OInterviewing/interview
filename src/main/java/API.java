import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class API {

    public API() {
    }

    public JsonObject getBuilder(String path, String searchquery) throws Exception {
        HttpGet httpGet;
        if (searchquery == null) {
            httpGet = new HttpGet("https://www.swapi.tech/api/" + path + "/");
        } else {
            httpGet = new HttpGet("https://www.swapi.tech/api/" + path + "/?search=" + searchquery);
        }
        return getRequest(httpGet);
    }

    public JsonObject getRequest(HttpGet getRequest) throws java.io.IOException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        getRequest.addHeader("accept", "application/json");
        HttpResponse response = httpClient.execute(getRequest);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        java.io.BufferedReader bufferedReader = new java.io.BufferedReader(
                new java.io.InputStreamReader((response.getEntity().getContent())));

        String line;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }

        JsonObject jsonObject = deserialize(stringBuilder.toString());
        bufferedReader.close();

        return jsonObject;
    }

    public JsonObject deserialize(String json) {
        Gson gson = new Gson();
        JsonObject jsonClass = gson.fromJson(json, JsonObject.class);
        return jsonClass;
    }

    public JsonObject innerRequest(String uri) throws java.io.IOException {
        HttpGet httpGet = new HttpGet(uri);
        return getRequest(httpGet);
    }
}
